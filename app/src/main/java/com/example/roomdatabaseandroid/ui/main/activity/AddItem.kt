package com.example.roomdatabaseandroid.ui.main.activity

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import com.example.roomdatabaseandroid.R
import com.example.roomdatabaseandroid.data.local.Database.AppdataBase
import com.example.roomdatabaseandroid.data.model.Task
import kotlinx.android.synthetic.main.activity_add_item.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

class AddItem : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_item)
        button_save.setOnClickListener(View.OnClickListener {
            GlobalScope.launch {
                saveData()
            }
        })

    }

    suspend fun saveData(){
        var edttask=editTextTask.text.toString()
        var edtdesc=editTextDesc.text.toString()
        var finishedby=editTextFinishBy.text.toString()

        if (edttask.isEmpty()) {
            editTextTask.setError("Task required");
            editTextTask.requestFocus();
            return;
        }

        if (edtdesc.isEmpty()) {
            editTextDesc.setError("Desc required");
            editTextDesc.requestFocus();
            return;
        }

        if (finishedby.isEmpty()) {
            editTextFinishBy.setError("Finish by required");
            editTextFinishBy.requestFocus();
            return;
        }

        GlobalScope.launch(Dispatchers.IO) {
            var task=Task()
            task.task=edttask
            task.desc=edtdesc
            task.finishBy=finishedby
            task.finished=false
            AppdataBase.getDatabaseInstance(applicationContext).appdao().insert(task)
        }
        startActivity(Intent(this, MainActivity::class.java))


    }
}