package com.example.roomdatabaseandroid.ui.main.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.roomdatabaseandroid.R
import com.example.roomdatabaseandroid.data.model.Task
import android.content.Intent
import com.example.roomdatabaseandroid.ui.main.activity.UpdateItem






class recyclerAdapter(var data:List<Task>,var context:Context): RecyclerView.Adapter<recyclerAdapter.MyViewHolder>() {
    class MyViewHolder(item: View):RecyclerView.ViewHolder(item) {
        var textViewTask:TextView?=null
        var textViewStatus:TextView?=null
        var textViewDesc:TextView?=null
        var textViewFinishBy:TextView?=null
        var update:TextView?=null


        init {
            textViewFinishBy=item.findViewById(R.id.textViewFinishBy)
            textViewDesc=item.findViewById(R.id.textViewDesc)
            textViewStatus=item.findViewById(R.id.textViewStatus)
            textViewTask=item.findViewById(R.id.textViewTask)
            update=item.findViewById(R.id.update)


        }

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        var view=LayoutInflater.from(parent.context).inflate(R.layout.recycler_item,parent,false)
        return MyViewHolder(view)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        holder.textViewTask!!.text=data.get(position).task
        holder.textViewDesc!!.text=data.get(position).desc
        holder.textViewFinishBy!!.text=data.get(position).finishBy

        holder.update!!.setOnClickListener(View.OnClickListener {
            val task: Task = data.get(position)
            val intent = Intent(context, UpdateItem::class.java)
            intent.putExtra("task", task)
            context.startActivity(intent)
        })
    }

    override fun getItemCount(): Int {
        return data.size
    }
}