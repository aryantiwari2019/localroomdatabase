package com.example.roomdatabaseandroid.ui.main.activity

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import androidx.appcompat.widget.SearchView
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.roomdatabaseandroid.ui.main.adapter.recyclerAdapter
import com.example.roomdatabaseandroid.R
import com.example.roomdatabaseandroid.data.local.Database.AppdataBase
import com.example.roomdatabaseandroid.data.model.Task
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.async
import kotlinx.coroutines.launch

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        locallist.layoutManager=LinearLayoutManager(this)


        searchview.addTextChangedListener(object : TextWatcher {

            override fun afterTextChanged(s: Editable) {
            }

            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                GlobalScope.launch {
                    getDatafromfilter(s.toString())
                }
            }
        })


        add.setOnClickListener(View.OnClickListener {
            startActivity(Intent(this, AddItem::class.java))
        })
        GlobalScope.launch {
            getData()
        }
    }

    suspend fun getData(){
       var data= GlobalScope.async(Dispatchers.IO) {
            AppdataBase.getDatabaseInstance(applicationContext).appdao().getAll()
        }

        locallist.adapter= recyclerAdapter(data = data.await()!!,this)
    }

    suspend fun getDatafromfilter(datas:String){
        var data= GlobalScope.async(Dispatchers.IO) {
            AppdataBase.getDatabaseInstance(applicationContext).appdao().getFilterData("%"+datas+"%")
        }

        var result=data.await()
        GlobalScope.launch(Dispatchers.Main) {
            setadapter(result!!)
        }


    }
    fun setadapter(data:List<Task>){
        locallist.adapter= recyclerAdapter(data =data!!,this)

    }
}