package com.example.roomdatabaseandroid.ui.main.activity

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import com.example.roomdatabaseandroid.R
import com.example.roomdatabaseandroid.data.local.Database.AppdataBase
import com.example.roomdatabaseandroid.data.model.Task
import kotlinx.android.synthetic.main.activity_update_item.*
import kotlinx.android.synthetic.main.activity_update_item.editTextDesc
import kotlinx.android.synthetic.main.activity_update_item.editTextFinishBy
import kotlinx.android.synthetic.main.activity_update_item.editTextTask
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch


class UpdateItem : AppCompatActivity() {
    var task: Task?=null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_update_item)
        getBundleData()
        init()
    }

    fun  init(){
        button_update.setOnClickListener(View.OnClickListener {
            GlobalScope.launch {
                updateData()
            }
        })
        button_delete.setOnClickListener(View.OnClickListener {
                GlobalScope.launch {
                    DeleteData()
                }
        })
    }

    fun  getBundleData(){
        task=intent.getParcelableExtra("task")
        loadTask(task!!)
    }

    private fun loadTask(task: Task) {
        editTextTask.setText(task.task)
        editTextDesc.setText(task.desc)
        editTextFinishBy.setText(task.finishBy)
        checkBoxFinished.setChecked(task.finished!!)
    }


    suspend fun updateData(){
        var edttask=editTextTask.text.toString()
        var edtdesc=editTextDesc.text.toString()
        var finishedby=editTextFinishBy.text.toString()

        if (edttask.isEmpty()) {
            editTextTask.setError("Task required");
            editTextTask.requestFocus();
            return;
        }

        if (edtdesc.isEmpty()) {
            editTextDesc.setError("Desc required");
            editTextDesc.requestFocus();
            return;
        }

        if (finishedby.isEmpty()) {
            editTextFinishBy.setError("Finish by required");
            editTextFinishBy.requestFocus();
            return;
        }

        GlobalScope.launch(Dispatchers.IO) {
            task!!.task=edttask
            task!!.desc=edtdesc
            task!!.finishBy=finishedby

            AppdataBase.getDatabaseInstance(applicationContext).appdao().update(task!!)
        }
        startActivity(Intent(this, MainActivity::class.java))

    }

    suspend fun DeleteData(){
        GlobalScope.launch(Dispatchers.IO) {
            AppdataBase.getDatabaseInstance(applicationContext).appdao().delete(task!!)
        }
        startActivity(Intent(this, MainActivity::class.java))

    }
}