package com.example.roomdatabaseandroid.data.local.dao

import androidx.room.*
import com.example.roomdatabaseandroid.data.model.Task


@Dao
public interface TaskDao {

    @Insert
    fun insert(task: Task)

    @Update
    fun  update(task: Task)

    @Delete
    fun delete(task: Task)

    @Query("SELECT * FROM task")
    fun getAll(): List<Task>?

    @Query("SELECT * FROM task WHERE finish_by LIKE :desc")
    fun getFilterData(desc:String):List<Task>?
}