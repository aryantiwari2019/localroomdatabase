package com.example.roomdatabaseandroid.data.model

import android.os.Parcel
import android.os.Parcelable
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "task")
class Task() :Parcelable {
    @PrimaryKey(autoGenerate = true)
     var id:Int?=null


    @ColumnInfo(name = "task")
     var task:String?=null

    @ColumnInfo(name = "desc")
     var desc:String?=null

    @ColumnInfo(name = "finish_by")
     var finishBy:String?=null

    @ColumnInfo(name = "finished")
     var finished:Boolean?=null

    constructor(parcel: Parcel) : this() {
        id = parcel.readValue(Int::class.java.classLoader) as? Int
        task = parcel.readString()
        desc = parcel.readString()
        finishBy = parcel.readString()
        finished = parcel.readValue(Boolean::class.java.classLoader) as? Boolean
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeValue(id)
        parcel.writeString(task)
        parcel.writeString(desc)
        parcel.writeString(finishBy)
        parcel.writeValue(finished)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Task> {
        override fun createFromParcel(parcel: Parcel): Task {
            return Task(parcel)
        }

        override fun newArray(size: Int): Array<Task?> {
            return arrayOfNulls(size)
        }
    }


}
