package com.example.roomdatabaseandroid.data.local.Database

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.example.roomdatabaseandroid.data.local.dao.TaskDao
import com.example.roomdatabaseandroid.data.model.Task

@Database(entities = [Task::class],version = 1)
 public abstract class AppdataBase: RoomDatabase() {
    companion object{
        var appdataBase: AppdataBase?=null
        val databasename:String="task_db"

        fun  getDatabaseInstance(context:Context): AppdataBase {
            if(appdataBase ==null){
                appdataBase = Room.databaseBuilder(context.applicationContext, AppdataBase::class.java, databasename).build()

            }

            return appdataBase as AppdataBase
        }
    }

    abstract fun appdao(): TaskDao

}